/* Horloge Binaire par Jean ELCHINGER optimis� par TheGuit
Date : 7/11/2003
Email : web.jean@ifrance.com

Script modifi� � partir de l'horloge binaire :
http://www.california.com/~binard

Pour acheter une vraie horloge binaire :
LED Binary Clock
http://www.thinkgeek.com/cubegoodies/lights/59e0/
*/

var timerID = null;

function showtime()
{
    
    var now = new Date();
    var decimal_hours = now.getHours();
    var decimal_minutes = now.getMinutes();
    var decimal_seconds = now.getSeconds();

    var sec=decimal_seconds;
    // seconde
    if(sec>=40)
    {
        document.clock.dsec2.checked = true;
        sec-=40;
    }
    else
    {
        document.clock.dsec2.checked = false;
    }
    if(sec>=20)
    {
        document.clock.dsec1.checked = true;
        sec-=20;
    }
    else
    {
        document.clock.dsec1.checked = false;
    }
    if(sec>=10)
    {
        document.clock.dsec0.checked = true;
        sec-=10;
    }
    else
    {
        document.clock.dsec0.checked = false;
    }
    if(sec>=8)
    {
        document.clock.usec3.checked = true;
        sec-=8;
    }
    else
    {
        document.clock.usec3.checked = false;
    }
    if(sec>=4)
    {
        document.clock.usec2.checked = true;
        sec-=4;
    }
    else
    {
        document.clock.usec2.checked = false;
    }
    if(sec>=2)
    {
        document.clock.usec1.checked = true;
        sec-=2;
    }
    else
    {
        document.clock.usec1.checked = false;
    }
     if(sec>=1)
    {
        document.clock.usec0.checked = true;
        sec-=1;    
    }
    else
    {
        document.clock.usec0.checked = false;
    }


    var min=decimal_minutes;
    // minute
    if(min>=40)
    {
        document.clock.dmin2.checked = true;
        min-=40;
    }
    else
    {
        document.clock.dmin2.checked = false;
    }
    if(min>=20)
    {
        document.clock.dmin1.checked = true;
        min-=20;
    }
    else
    {
        document.clock.dmin1.checked = false;
    }
    if(min>=10)
    {
        document.clock.dmin0.checked = true;
        min-=10;
    }
    else
    {
        document.clock.dmin0.checked = false;
    }
    if(min>=8)
    {
        document.clock.umin3.checked = true;
        min-=8;
    }
    else
    {
        document.clock.umin3.checked = false;
    }
    if(min>=4)
    {
        document.clock.umin2.checked = true;
        min-=4;
    }
    else
    {
        document.clock.umin2.checked = false;
    }
    if(min>=2)
    {
        document.clock.umin1.checked = true;
        min-=2;
    }
    else
    {
        document.clock.umin1.checked = false;
    }
     if(min>=1)
    {
        document.clock.umin0.checked = true;
        min-=1;    
    }
    else
    {
        document.clock.umin0.checked = false;
    }


    var heure=decimal_hours;
    // heure
    if(heure>=20)
    {
        document.clock.dheure1.checked = true;
        heure-=20;
    }
    else
    {
        document.clock.dheure1.checked = false;
    }
    if(heure>=10)
    {
        document.clock.dheure0.checked = true;
        heure-=10;
    }
    else
    {
        document.clock.dheure0.checked = false;
    }
    if(heure>=8)
    {
        document.clock.uheure3.checked = true;
        heure-=8;
    }
    else
    {
        document.clock.uheure3.checked = false;
    }
    if(heure>=4)
    {
        document.clock.uheure2.checked = true;
        heure-=4;
    }
    else
    {
        document.clock.uheure2.checked = false;
    }
    if(heure>=2)
    {
        document.clock.uheure1.checked = true;
        heure-=2;
    }
    else
    {
        document.clock.uheure1.checked = false;
    }
     if(heure>=1)
    {
        document.clock.uheure0.checked = true;
        heure-=1;    
    }
    else
    {
        document.clock.uheure0.checked = false;
    } 

    timerID = setTimeout("showtime();",1000);
}